use regex::{CaptureMatches, Regex};
use std::result;
use std::num::ParseIntError;
use std::str::FromStr;
use std::collections::HashMap;

const OPS_TABLE: [(&'static str, Op); 5] = [("+", Op::Plus),
                                            ("-", Op::Minus),
                                            ("*", Op::Times),
                                            ("/", Op::Divide),
                                            ("^", Op::Exponent)];

lazy_static! {
    static ref REGEX: Regex = Regex::new(r###"\s*(?:(\d+)|(.))"###).unwrap();
    static ref OPS: HashMap<&'static str, Op> = {
        let mut map = HashMap::new();
        for &(name, op) in OPS_TABLE.iter() {
            map.insert(name, op);
        }

        map
    };
}

#[derive(Debug)]
pub enum TokenErr {
    InvalidToken,
    ParseIntError(ParseIntError),
}

impl From<ParseIntError> for TokenErr {
    fn from(err: ParseIntError) -> TokenErr {
        TokenErr::ParseIntError(err)
    }
}

pub type TokenResult<T> = result::Result<T, TokenErr>;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Op {
    Plus,
    Minus,
    Times,
    Divide,
    Exponent,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Token {
    BinOp(Op),
    Num(i32),
    LeftParen,
    RightParen,
}

pub struct Tokenizer<'a> {
    curr_token: Option<Token>,
    tokens: CaptureMatches<'static, 'a>,
}

impl<'a> Tokenizer<'a> {
    pub fn new(text: &'a str) -> Tokenizer<'a> {
        Tokenizer {
            curr_token: None,
            tokens: REGEX.captures_iter(text),
        }
    }

    pub fn curr_token(&self) -> Option<Token> {
        self.curr_token
    }

    pub fn get_next_token(&mut self) -> TokenResult<()> {
        if let Some(token) = self.tokens.next() {
            let token = match (token.get(1), token.get(2)) {
                (Some(num), _) => Token::Num(i32::from_str(num.as_str())?),
                (_, Some(op)) if op.as_str() == "(" => Token::LeftParen,
                (_, Some(op)) if op.as_str() == ")" => Token::RightParen,
                (_, Some(op)) if OPS.contains_key(op.as_str()) => {
                    let op = OPS.get(op.as_str()).unwrap().clone();
                    Token::BinOp(op)
                }
                _ => return Err(TokenErr::InvalidToken),
            };

            self.curr_token = Some(token);
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::{Op, Token, Tokenizer};

    #[test]
    fn test_tokens() {
        let mut tokenizer = Tokenizer::new("1 + 2 * 3");
        tokenizer.get_next_token().unwrap();
        assert_eq!(tokenizer.curr_token(), Some(Token::Num(1)));
        tokenizer.get_next_token().unwrap();
        assert_eq!(tokenizer.curr_token(), Some(Token::BinOp(Op::Plus)));
        tokenizer.get_next_token().unwrap();
        assert_eq!(tokenizer.curr_token(), Some(Token::Num(2)));
        tokenizer.get_next_token().unwrap();
        assert_eq!(tokenizer.curr_token(), Some(Token::BinOp(Op::Times)));
        tokenizer.get_next_token().unwrap();
        assert_eq!(tokenizer.curr_token(), Some(Token::Num(3)));
    }
}
