#[macro_use]
extern crate lazy_static;
extern crate regex;

pub mod tokenizer;
pub mod exp;
pub mod parser;
