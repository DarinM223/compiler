use tokenizer::Op;

#[derive(Debug, PartialEq)]
pub enum Value {
    Num(i32),
}

#[derive(Debug, PartialEq)]
pub enum Expr {
    Value(Value),
    BinOp(Op, Box<Expr>, Box<Expr>),
}
