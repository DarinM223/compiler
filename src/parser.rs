use exp::{Expr, Value};
use tokenizer::{Op, Token, Tokenizer, TokenErr};
use std::collections::HashMap;
use std::result;

#[derive(Clone, Copy, PartialEq, Eq)]
enum Assoc {
    Left,
    Right,
}

const OP_INFO_TABLE: [(Op, (i32, Assoc)); 5] = [(Op::Plus, (1, Assoc::Left)),
                                                (Op::Minus, (1, Assoc::Left)),
                                                (Op::Times, (2, Assoc::Left)),
                                                (Op::Divide, (2, Assoc::Left)),
                                                (Op::Exponent, (3, Assoc::Right))];
lazy_static! {
    static ref OP_INFO: HashMap<Op, (i32, Assoc)> = {
        let mut map = HashMap::new();
        for &(op, tuple) in OP_INFO_TABLE.iter() {
            map.insert(op, tuple);
        }

        map
    };
}

#[derive(Debug)]
pub enum ParseErr {
    UnexpectedEnd,
    ExpectedValue,
    UnmatchedParen,
    TokenErr(TokenErr),
}

impl From<TokenErr> for ParseErr {
    fn from(err: TokenErr) -> ParseErr {
        ParseErr::TokenErr(err)
    }
}

pub type ParseResult<T> = result::Result<T, ParseErr>;

pub struct Parser<'a> {
    tokenizer: Tokenizer<'a>,
}

impl<'a> Parser<'a> {
    pub fn new(text: &'a str) -> ParseResult<Parser<'a>> {
        let mut tokenizer = Tokenizer::new(text);
        tokenizer.get_next_token()?;
        Ok(Parser { tokenizer: tokenizer })
    }

    pub fn parse_value(&mut self) -> ParseResult<Expr> {
        let token = self.tokenizer.curr_token();
        match token {
            Some(Token::LeftParen) => {
                self.tokenizer.get_next_token()?;
                let value_expr = self.parse_expr(1)?;
                if self.tokenizer.curr_token() != Some(Token::RightParen) {
                    return Err(ParseErr::UnmatchedParen);
                }
                self.tokenizer.get_next_token()?;
                Ok(value_expr)
            }
            Some(Token::BinOp(..)) => Err(ParseErr::ExpectedValue),
            Some(Token::RightParen) => Err(ParseErr::ExpectedValue),
            Some(Token::Num(num)) => {
                self.tokenizer.get_next_token()?;
                Ok(Expr::Value(Value::Num(num)))
            }
            None => Err(ParseErr::UnexpectedEnd),
        }
    }

    pub fn parse_expr(&mut self, min_prec: i32) -> ParseResult<Expr> {
        let mut left_value = self.parse_value()?;

        loop {
            let token = self.tokenizer.curr_token();
            let op;
            match token {
                Some(Token::BinOp(ref op)) if OP_INFO[op].0 < min_prec => break,
                Some(Token::BinOp(bin_op)) => op = bin_op,
                None => break,
                _ => break,
            }

            let (prec, assoc) = OP_INFO[&op];
            let next_min_prec = if assoc == Assoc::Left { prec + 1 } else { prec };

            self.tokenizer.get_next_token()?;
            let right_value = self.parse_expr(next_min_prec)?;

            left_value = Expr::BinOp(op, Box::new(left_value), Box::new(right_value));
        }

        Ok(left_value)
    }
}

#[cfg(test)]
mod tests {
    use super::Parser;
    use exp::{Expr, Value};
    use tokenizer::Op;

    #[test]
    fn test_parser() {
        let inputs = ["1 + 2 * 3",
                      "1 + 2 + 3",
                      "1 ^ 2 ^ 3",
                      "1 * (2 + 3)",
                      "1 + 2 * 3 + 4"];

        let nested_three_ops =
            Box::new(Expr::BinOp(Op::Plus,
                                 Box::new(Expr::Value(Value::Num(1))),
                                 Box::new(Expr::BinOp(Op::Times,
                                                      Box::new(Expr::Value(Value::Num(2))),
                                                      Box::new(Expr::Value(Value::Num(3)))))));
        let three_ops = Expr::BinOp(Op::Plus,
                                    nested_three_ops,
                                    Box::new(Expr::Value(Value::Num(4))));

        let outputs = [Expr::BinOp(Op::Plus,
                                   Box::new(Expr::Value(Value::Num(1))),
                                   Box::new(Expr::BinOp(Op::Times,
                                                        Box::new(Expr::Value(Value::Num(2))),
                                                        Box::new(Expr::Value(Value::Num(3)))))),
                       Expr::BinOp(Op::Plus,
                                   Box::new(Expr::BinOp(Op::Plus,
                                                        Box::new(Expr::Value(Value::Num(1))),
                                                        Box::new(Expr::Value(Value::Num(2))))),
                                   Box::new(Expr::Value(Value::Num(3)))),
                       Expr::BinOp(Op::Exponent,
                                   Box::new(Expr::Value(Value::Num(1))),
                                   Box::new(Expr::BinOp(Op::Exponent,
                                                        Box::new(Expr::Value(Value::Num(2))),
                                                        Box::new(Expr::Value(Value::Num(3)))))),
                       Expr::BinOp(Op::Times,
                                   Box::new(Expr::Value(Value::Num(1))),
                                   Box::new(Expr::BinOp(Op::Plus,
                                                        Box::new(Expr::Value(Value::Num(2))),
                                                        Box::new(Expr::Value(Value::Num(3)))))),
                       three_ops];
        for (input, output) in inputs.iter().zip(outputs.iter()) {
            let mut parser = Parser::new(input).unwrap();
            let expr = parser.parse_expr(1);
            assert_eq!(expr.unwrap(), *output);
        }
    }
}
